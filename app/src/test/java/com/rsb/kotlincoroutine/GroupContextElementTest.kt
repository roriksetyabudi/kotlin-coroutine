package com.rsb.kotlincoroutine

import kotlinx.coroutines.*
import org.junit.jupiter.api.Test
import java.util.concurrent.Executor
import java.util.concurrent.Executors

class GroupContextElementTest {
    @Test
    fun testGroupContextElement(){
        val dispatcher = Executors.newFixedThreadPool(10).asCoroutineDispatcher()
        val scope = CoroutineScope(Dispatchers.IO + CoroutineName("Test"))
        val job = scope.launch(CoroutineName("Parent") + dispatcher) {
            println("Parent Run In Thread : ${Thread.currentThread().name}")
            withContext(CoroutineName("Child") + Dispatchers.IO) {
                println("Child Run In Thread : ${Thread.currentThread().name}")
            }
        }
        runBlocking {
            job.join()
        }
    }
}