package com.rsb.kotlincoroutine

import kotlinx.coroutines.*
import kotlinx.coroutines.channels.ConflatedBroadcastChannel
import org.junit.jupiter.api.Test

class ConflatedBroadcastChannelTest {
    @Test
    fun testConflatedBroadcastChannel(){
        val conflatedBroadcastChannel = ConflatedBroadcastChannel<Int>()
        val receiveChannel = conflatedBroadcastChannel.openSubscription()

        val scope = CoroutineScope(Dispatchers.IO)

        val job1 = scope.launch {
            repeat(10){
                delay(1000)
                println("Send $it")
                conflatedBroadcastChannel.send(it)
            }
        }
        val job2 = scope.launch {
            repeat(10) {
                delay(2000)
                println("Receive ${receiveChannel.receive()}")
            }
        }
        runBlocking {
            //joinAll(job1, job2)
            delay(11_000)
            scope.cancel()
        }

    }
}