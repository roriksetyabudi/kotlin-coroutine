package com.rsb.kotlincoroutine

import kotlinx.coroutines.*
import org.junit.jupiter.api.Test
import java.lang.IllegalArgumentException
import java.util.concurrent.Executor
import java.util.concurrent.Executors

class JobEceptionHandlerTest {
    @Test
    fun testJobExceptionHandler(){
        val exceptionHandler = CoroutineExceptionHandler { coroutineContext, throwable ->
            println("Error : ${throwable.message}")
        }
        val dispatcher = Executors.newFixedThreadPool(10).asCoroutineDispatcher()
        val scope = CoroutineScope(dispatcher)

        runBlocking {
            val job = scope.launch { //not use
                launch(exceptionHandler) {
                    println("Job Child")
                    throw IllegalArgumentException("Child Error")
                }
            }
            job.join()
        }

    }
    @Test
    fun testSupervisorJobExceptionHandler(){
        val exceptionHandler = CoroutineExceptionHandler { coroutineContext, throwable ->
            println("Error : ${throwable.message}")
        }
        val dispatcher = Executors.newFixedThreadPool(10).asCoroutineDispatcher()
        val scope = CoroutineScope(dispatcher)

        runBlocking {
            val job = scope.launch { //not use
                supervisorScope {
                    launch(exceptionHandler) {
                        println("Job Child")
                        throw IllegalArgumentException("Child Error")
                    }
                }
            }
            job.join()
        }

    }
}