package com.rsb.kotlincoroutine

import kotlinx.coroutines.*
import org.junit.jupiter.api.Test

class AwaitCancelationTest {
    @Test
    fun testAwaitCancellation(){
        runBlocking {
            val job = launch {
                try {
                    println("Job Start")
                    awaitCancellation()
                } finally {
                    println("Cancelled")
                }
            }
            delay(5000)
            job.cancelAndJoin()
        }
    }
}