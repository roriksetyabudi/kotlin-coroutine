package com.rsb.kotlincoroutine

import kotlinx.coroutines.*
import org.junit.jupiter.api.Test
import java.lang.IllegalArgumentException
import java.util.concurrent.Executor
import java.util.concurrent.Executors

class SuperVisiorJobTest {
    @Test
    fun testJob(){
        val dispatcher = Executors.newFixedThreadPool(10).asCoroutineDispatcher()
        val scope = CoroutineScope(dispatcher + Job())
        val job1 = scope.launch {
            delay(2000)
            println("Job 1 Done")
        }
        val job2 = scope.launch {
            delay(1000)
            throw IllegalArgumentException("Job 2 failed")
        }
        runBlocking {
            joinAll(job1, job2)
        }
    }

    @Test
    fun testSuperVisiorJob(){
        val dispatcher = Executors.newFixedThreadPool(10).asCoroutineDispatcher()
        val scope = CoroutineScope(dispatcher + SupervisorJob())
        val job1 = scope.launch {
            delay(2000)
            println("Job 1 Done")
        }
        val job2 = scope.launch {
            delay(1000)
            throw IllegalArgumentException("Job 2 failed")
        }
        runBlocking {
            joinAll(job1, job2)
        }
    }
}