package com.rsb.kotlincoroutine

import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Test

class ChannelUndeliveredElementTest {
    @Test
    fun testUndeliveredElement(){
        val channel = Channel<Int>(capacity = 10) {
            println("Undelivered Element $it")
        }
        channel.close()
        runBlocking {
            val job = launch {
                channel.send(10)
                channel.send(100)
            }
            job.join()
        }
    }
}