package com.rsb.kotlincoroutine

import kotlinx.coroutines.*
import org.junit.jupiter.api.Test

class CoroutineScopeFunctionTest {
    suspend fun getFoo(): Int{
        delay(100)
        println("Fo : ${Thread.currentThread().name}")
        return 10
    }
    suspend fun getBar(): Int {
        delay(100)
        println("Bar : ${Thread.currentThread().name}")
        return 10
    }
    suspend fun getSum(): Int = coroutineScope {
        val foo = async { getFoo() }
        val bar = async { getBar() }
        foo.await() + bar.await()
    }

    @Test
    fun testCoroutineScopeFunction(){
        val scope = CoroutineScope(Dispatchers.IO)
        val job = scope.launch {
            val result = getSum()
            println("Result : $result")
        }
        runBlocking {
            job.join()
        }
    }
}