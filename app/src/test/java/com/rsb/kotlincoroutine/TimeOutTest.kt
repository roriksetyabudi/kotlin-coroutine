package com.rsb.kotlincoroutine

import kotlinx.coroutines.*
import org.junit.jupiter.api.Test
import java.util.*

class TimeOutTest {

    @Test
    fun testTimeout(){
        runBlocking {
            val job = GlobalScope.launch {
                println("Start Coroutine Time Out")
                withTimeout(5000) {
                    repeat(100){
                        delay(100)
                        println("$it : ${Date()}")
                    }
                }
                println("Finish Coroutine")
            }
            job.join()
        }
    }
    @Test
    fun testTimeoutOrNull(){
        runBlocking {
            val job = GlobalScope.launch {
                println("Start Coroutine Time Out")
                withTimeoutOrNull(5000) {
                    repeat(100){
                        delay(100)
                        println("$it : ${Date()}")
                    }
                }
                println("Finish Coroutine")
            }
            job.join()
        }
    }
}