package com.rsb.kotlincoroutine

import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Test
import java.util.*

class SuspendFunctionTest {
    suspend fun helloWord(){
        println("Hallo : ${Date()} : Theread ${Thread.currentThread().name}")
        delay(2_000)
        println("Word : ${Date()} : Theread ${Thread.currentThread().name}")
    }
    @Test
    fun testSuspendFunction(){
        runBlocking {
            helloWord()
        }
    }
}