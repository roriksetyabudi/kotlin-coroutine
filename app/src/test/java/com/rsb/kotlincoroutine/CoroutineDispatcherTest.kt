package com.rsb.kotlincoroutine

import kotlinx.coroutines.*
import org.junit.jupiter.api.Test
import java.util.concurrent.Executor
import java.util.concurrent.Executors

class CoroutineDispatcherTest {
    @Test
    fun testDispatcher(){
        runBlocking {
            println("runBlocking ${Thread.currentThread().name}")

            val job1 = GlobalScope.launch(Dispatchers.Default) {
                println("Jobs 1 ${Thread.currentThread().name}")
            }
            val job2 = GlobalScope.launch(Dispatchers.IO) {
                println("Jobs 2 ${Thread.currentThread().name}")
            }
            joinAll(job1, job2)
        }
    }
    @Test
    fun testUnconfined(){
        runBlocking {
            println("RUnblocking ${Thread.currentThread().name}")
            GlobalScope.launch(Dispatchers.Unconfined) {
                println("Uncofined : ${Thread.currentThread().name}")
                delay(1_000)
                println("Uncofined : ${Thread.currentThread().name}")
                delay(1_000)
                println("Uncofined : ${Thread.currentThread().name}")
            }
            GlobalScope.launch {
                println("Confined : ${Thread.currentThread().name}")
                delay(1_000)
                println("Confined : ${Thread.currentThread().name}")
                delay(1_000)
                println("Confined : ${Thread.currentThread().name}")
            }
            delay(3_000)
        }
    }

    @Test
    fun testExecutorService(){
        val dispatcherService = Executors.newFixedThreadPool(10).asCoroutineDispatcher()
        val dispatcherWeb = Executors.newSingleThreadExecutor().asCoroutineDispatcher()

        runBlocking {
            val job1 = GlobalScope.launch(dispatcherService) {
                println("Job 1 : ${Thread.currentThread().name}")
            }
            val job2 = GlobalScope.launch(dispatcherWeb) {
                println("Job 2 : ${Thread.currentThread().name}")
            }
            joinAll(job1, job2)
        }
    }
}