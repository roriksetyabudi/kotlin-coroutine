package com.rsb.kotlincoroutine

import kotlinx.coroutines.*
import kotlinx.coroutines.flow.*
import org.junit.jupiter.api.Test
import java.util.*

class StateFlow {
    @Test
    fun testStateFLow(){
        val scope = CoroutineScope(Dispatchers.IO)
        val stateFlow = MutableStateFlow(0)

        scope.launch {
            repeat(10) {
                println("      Send $it : ${Date()}")
                stateFlow.emit(it)
                delay(1000)

            }
        }
        scope.launch {
            stateFlow.asStateFlow()
                .map { "Receive Job2 : $it : ${Date()}" }
                .collect {
                    delay(2000)
                    println(it)
                }
        }
        runBlocking {
            delay(21_000)
            scope.cancel()
        }
    }
}