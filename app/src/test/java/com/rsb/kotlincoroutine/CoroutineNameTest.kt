package com.rsb.kotlincoroutine

import kotlinx.coroutines.*
import org.junit.jupiter.api.Test

class CoroutineNameTest {
    @Test
    fun testCoroutineName(){
        val scope = CoroutineScope(Dispatchers.IO)
        val job = scope.launch(CoroutineName("Parent")) {
            println("Parent Run In Thread : ${Thread.currentThread().name}")
            withContext(CoroutineName("Child")) {
                println("Child Run In Thread : ${Thread.currentThread().name}")
            }
        }
        runBlocking {
            job.join()
        }
    }
}