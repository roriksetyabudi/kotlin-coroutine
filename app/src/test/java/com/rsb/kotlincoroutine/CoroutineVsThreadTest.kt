package com.rsb.kotlincoroutine

import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Test
import java.util.*
import kotlin.concurrent.thread

class CoroutineVsThreadTest {
    @Test
    fun testThread(){
        repeat(100000){
            thread {
                Thread.sleep(1_000)
                println("Done $it : ${Date()} : ${Thread.currentThread().name} ")
            }
        }
        println("Menunggu")
        Thread.sleep(3_000)
        println("Finish Selesai")
    }
    @Test
    fun testCoroutine(){
        repeat(100000) {
            GlobalScope.launch {
                delay(1000)
                println("Done $it : ${Date()} : ${Thread.currentThread().name}")
            }
        }
        println("Menunggu")
        runBlocking {
            delay(3_000)
        }
        println("Finish Selesai")
    }
}