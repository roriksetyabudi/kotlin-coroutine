package com.rsb.kotlincoroutine

import kotlinx.coroutines.*
import org.junit.jupiter.api.Test
import kotlin.system.measureTimeMillis

class AwaitAllFunctionTest {
    suspend fun getFoo(): Int{
        delay(1_000)
        return 10
    }
    suspend fun getBar(): Int {
        delay(1_000)
        return 10
    }

    @Test
    fun testAwaitAllFunction(){
        runBlocking {
            val time = measureTimeMillis {
                val foo1: Deferred<Int> = GlobalScope.async { getFoo() }
                val bar1: Deferred<Int> = GlobalScope.async { getBar() }
                val foo2: Deferred<Int> = GlobalScope.async { getFoo() }
                val bar2: Deferred<Int> = GlobalScope.async { getBar() }
                val result = awaitAll(foo1,foo2,bar1,bar2).sum()
                println("Result $result")
            }
            println("Total Time $time")

        }
    }
}