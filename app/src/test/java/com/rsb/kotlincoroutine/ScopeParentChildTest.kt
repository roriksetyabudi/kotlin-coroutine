package com.rsb.kotlincoroutine

import kotlinx.coroutines.*
import org.junit.jupiter.api.Test
import java.util.concurrent.Executor
import java.util.concurrent.Executors

class ScopeParentChildTest {
    @Test
    fun testScopeParentChildDispatcher(){
        val dispatcher = Executors.newFixedThreadPool(10).asCoroutineDispatcher()
        val scope = CoroutineScope(dispatcher)

        val job = scope.launch {
            println("Parent Scope : ${Thread.currentThread().name}")
            coroutineScope {
                launch {
                    println("Child Scope : ${Thread.currentThread().name}")
                }
            }
        }
        runBlocking {
            job.join()
        }
    }
    @Test
    fun testScopeParentChildDispatcherCancel(){
        val dispatcher = Executors.newFixedThreadPool(10).asCoroutineDispatcher()
        val scope = CoroutineScope(dispatcher)

        val job = scope.launch {
            println("Parent Scope : ${Thread.currentThread().name}")
            coroutineScope {
                launch {
                    delay(2_000)
                    println("Child Scope : ${Thread.currentThread().name}")
                }
            }
        }
        runBlocking {
            job.cancelAndJoin()
        }
    }
}