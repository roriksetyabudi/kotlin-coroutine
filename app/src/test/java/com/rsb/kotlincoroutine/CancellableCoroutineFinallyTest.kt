package com.rsb.kotlincoroutine

import kotlinx.coroutines.*
import org.junit.jupiter.api.Test
import java.util.*

class CancellableCoroutineFinallyTest {
    @Test
    fun testCancellableFinaly(){
        runBlocking {
            val job = GlobalScope.launch {
                try {
                    println("Start Coroutine ${Date()}")
                    delay(2_000)
                    println("End Coroutine ${Date()}")
                } finally {
                    println("Finish")
                }
            }
            job.cancelAndJoin()

        }
    }
}