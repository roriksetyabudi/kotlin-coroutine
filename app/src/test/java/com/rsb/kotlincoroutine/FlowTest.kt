package com.rsb.kotlincoroutine

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Test


class FlowTest {
    @Test
    fun testFlow(){
        val flow1: Flow<Int> = flow {
            println("Star Flow")
            repeat(100) {
                println("Emit It")
                emit(it)
            }
        }
        runBlocking {
            flow1.collect {
                println("Receive It : $it")
            }
        }
    }
}