package com.rsb.kotlincoroutine

import kotlinx.coroutines.*
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.channels.ReceiveChannel
import kotlinx.coroutines.channels.produce
import org.junit.jupiter.api.Test

class ProduceTest {
    @Test
    fun testProduce(){
        val scope = CoroutineScope(Dispatchers.IO)

        val channel: ReceiveChannel<Int> = scope.produce {
            repeat(100) {
                send(it)
            }
        }
        val job1 = scope.launch {
            repeat(100) {
                println(channel.receive())
            }
        }
        runBlocking {
            joinAll(job1)
        }


    }


}