package com.rsb.kotlincoroutine

import kotlinx.coroutines.*
import org.junit.jupiter.api.Test

class CoroutineScopeTest {
    @Test
    fun testScope(){
        val scope = CoroutineScope(Dispatchers.IO)
        scope.launch {
            delay(2000)
            println("Run ${Thread.currentThread().name}")

        }
        scope.launch {
            delay(2000)
            println("Run ${Thread.currentThread().name}")

        }
        runBlocking {
            delay(1000)
            println("Done")
        }

    }
    @Test
    fun testScopeCancel(){
        val scope = CoroutineScope(Dispatchers.IO)
        scope.launch {
            delay(2000)
            println("Run ${Thread.currentThread().name}")

        }
        scope.launch {
            delay(2000)
            println("Run ${Thread.currentThread().name}")

        }
        runBlocking {
            delay(1000)
            scope.cancel()
            delay(2000)
            println("Done")
        }

    }
}