package com.rsb.kotlincoroutine

import kotlinx.coroutines.*
import org.junit.jupiter.api.Test
import java.lang.IllegalArgumentException
import java.util.concurrent.Executor
import java.util.concurrent.Executors

class SuperVisorScope {
    @Test
    fun testSuperVisorScope(){
        val dispatcher = Executors.newFixedThreadPool(10).asCoroutineDispatcher()
        val scope = CoroutineScope(dispatcher + Job())
        runBlocking {
            scope.launch {
                supervisorScope {
                    launch {
                        delay(2000)
                        println("Child 2 DIne")
                    }
                    launch {
                        delay(1000)
                        throw IllegalArgumentException("Child 2 error")
                    }
                }
            }
            delay(1000)
        }
    }
}