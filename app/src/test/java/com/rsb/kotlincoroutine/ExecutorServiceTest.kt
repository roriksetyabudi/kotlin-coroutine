package com.rsb.kotlincoroutine

import org.junit.jupiter.api.Test
import java.util.*
import java.util.concurrent.Executor
import java.util.concurrent.Executors

class ExecutorServiceTest {
    @Test
    fun testSingleThreadPool(){
        val executorService = Executors.newSingleThreadExecutor()
        repeat(10){
            val runnable = Runnable {
                Thread.sleep(1_000)
                println("Done $it ${Thread.currentThread().name} : ${Date()}")
            }
            executorService.execute(runnable)
            println("Selesai Memasukan Runnable $it")
        }
        println("Menunggu")
        Thread.sleep(11_000)
        println("Finish")
    }
    @Test
    fun testFixThreadPool(){
        val executorService = Executors.newFixedThreadPool(3)
        repeat(10){
            val runnable = Runnable {
                Thread.sleep(1_000)
                println("Done $it ${Thread.currentThread().name} : ${Date()}")
            }
            executorService.execute(runnable)
            println("Selesai Memasukan Runnable $it")
        }
        println("Menunggu")
        Thread.sleep(11_000)
        println("Finish")
    }
    @Test
    fun testCacheThreadPool(){
        val executorService = Executors.newCachedThreadPool()
        repeat(10){
            val runnable = Runnable {
                Thread.sleep(1_000)
                println("Done $it ${Thread.currentThread().name} : ${Date()}")
            }
            executorService.execute(runnable)
            println("Selesai Memasukan Runnable $it")
        }
        println("Menunggu")
        Thread.sleep(11_000)
        println("Finish")
    }
}