package com.rsb.kotlincoroutine

import kotlinx.coroutines.*
import org.junit.jupiter.api.Test
import kotlin.system.measureTimeMillis

class AsyncFunctionTest {
    suspend fun getFoo(): Int{
        delay(1_000)
        return 10
    }
    suspend fun getBar(): Int {
        delay(1_000)
        return 10
    }
    @Test
    fun testAync(){
        runBlocking {
            val time = measureTimeMillis {
                val foo: Deferred<Int> = GlobalScope.async { getFoo() }
                val bar: Deferred<Int> = GlobalScope.async { getBar() }
                val result = foo.await() + bar.await()
                println("Result $result")
            }
            println("Total Time $time")

        }
    }
}