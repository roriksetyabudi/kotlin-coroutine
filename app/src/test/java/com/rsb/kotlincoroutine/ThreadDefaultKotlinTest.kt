package com.rsb.kotlincoroutine

import org.junit.jupiter.api.Test
import java.util.*
import kotlin.concurrent.thread

class ThreadDefaultKotlinTest {
    @Test
    fun testDefaultKotlin(){
      thread(start = true) {
          println(Date())
          Thread.sleep(2_000)
          println("Finish : ${Date()}")
      }
        println("Menunggu Selesai")
        Thread.sleep(3_000)
        println("Selesai ")
    }
}