package com.rsb.kotlincoroutine

import kotlinx.coroutines.*
import org.junit.jupiter.api.Test
import java.util.concurrent.Executor
import java.util.concurrent.Executors

class YieldTest {
    suspend fun runJob(number: Int) {
        println("Start Job $number in thread :${Thread.currentThread().name}")
        yield()
        println("End Job $number in thread :${Thread.currentThread().name}")
    }
    @Test
    fun testYieldFunction(){
        var dispatcher = Executors.newFixedThreadPool(10).asCoroutineDispatcher()
        val scope = CoroutineScope(dispatcher)
        runBlocking {
            scope.launch { runJob(1) }
            scope.launch { runJob(2) }

            delay(2000)
        }
    }
}