package com.rsb.kotlincoroutine

import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Test

class CoroutineTest {
    suspend fun hello(){
        delay(1_000)
        println("Hello belajar kotlin coroutine")
    }
    @Test
    fun testCoroutine(){
        GlobalScope.launch {
            //silahkan masukan kode program anda di sini
            hello()
        }
        println("Menunggu")
        runBlocking {
            delay(2_000)
        }
        println("SELESAI")
    }
}