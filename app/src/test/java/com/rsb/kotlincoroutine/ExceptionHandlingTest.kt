package com.rsb.kotlincoroutine

import kotlinx.coroutines.*
import org.junit.jupiter.api.Test
import java.lang.IllegalArgumentException

class ExceptionHandlingTest {
    @Test
    fun testExceptionLaunch(){
        runBlocking {
            val job = GlobalScope.launch {
                println("Start Coroutine")
                throw IllegalArgumentException()
            }
            job.join()
            println("Finish")
        }
    }
    @Test
    fun testExceptionAsync(){
       runBlocking {
           val deferred = GlobalScope.async {
               println("Start Coroutine")
               throw IllegalArgumentException()
           }
           try {
               deferred.await()
           } catch (error: IllegalArgumentException) {
               println("Error")
           } finally {
               println("Finish")
           }

       }
    }
    @Test
    fun testExceptionHandler(){
        val exceptionHandler = CoroutineExceptionHandler { coroutineContext, throwable ->
            println("Ups Error : ${throwable.message}")
        }
        val scope = CoroutineScope(Dispatchers.IO + exceptionHandler)
        runBlocking {
            val job1 = GlobalScope.launch(exceptionHandler) {
                println("Start Coroutine")
                throw IllegalArgumentException("Error")
            }
            job1.join()
            println("Finish")

            val job2 = scope.launch {
                println("Start Coroutine")
                throw IllegalArgumentException("Error")
            }
            job2.join()
            println("Finish")
        }
    }

}