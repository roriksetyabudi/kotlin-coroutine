package com.rsb.kotlincoroutine

import kotlinx.coroutines.*
import org.junit.jupiter.api.Test

class CancelChildrenTest {
    @Test
    fun testCancelChildren(){
        runBlocking {
            val job = GlobalScope.launch {
                launch {
                    delay(2000)
                    println("Child 1 Done")
                }
                launch {
                    delay(4000)
                    println("Child 2 Done")
                }
                launch {
                    delay(3000)
                    println("Child 3 Done")
                }
                delay(1000)
                println("Parent Done")
            }
            job.cancelChildren()
            job.join()
        }
    }
}