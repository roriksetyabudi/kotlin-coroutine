package com.rsb.kotlincoroutine

import kotlinx.coroutines.*
import org.junit.jupiter.api.Test

class NonCancellaleContextTest {
    @Test
    fun testCancelFinally(){
        runBlocking {
            val job = GlobalScope.launch {
                try {
                    println("Start Job")
                    delay(1000)
                    println("end Job")
                } finally {
                    println(isActive)
                    delay(1000)
                    println("Finally")
                }
            }
            job.cancelAndJoin()
        }
    }
    @Test
    fun testCancellable(){
        runBlocking {
            val job = GlobalScope.launch {
                try {
                    println("Start Job")
                    delay(1000)
                    println("end Job")
                } finally {
                    withContext(NonCancellable) {
                        println(isActive)
                        delay(1000)
                        println("Finally")
                    }
                }
            }
            job.cancelAndJoin()
        }

    }
}