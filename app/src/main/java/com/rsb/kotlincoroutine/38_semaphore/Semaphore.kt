package com.rsb.kotlincoroutine.`38_semaphore`

import kotlinx.coroutines.*
import kotlinx.coroutines.sync.Semaphore
import kotlinx.coroutines.sync.withPermit
import java.util.concurrent.Executors

fun main() {
    var counter: Int = 0
    val dispatcher = Executors.newFixedThreadPool(10).asCoroutineDispatcher()
    val scope = CoroutineScope(dispatcher)
    val semaphore = Semaphore(permits = 2) //permits semakin tinggi semakin tidak valid


    repeat(100) {
        scope.launch {
            repeat(1000) {
                semaphore.withPermit {
                    counter++
                }

            }
        }
    }
    runBlocking {
        delay(5000)
        println("Total Counter : $counter")
    }
}