package com.rsb.kotlincoroutine.`45_channel_buffer_overflow`

import kotlinx.coroutines.channels.BufferOverflow
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.joinAll
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking



fun testChannelBufferOverflowSuspend(){
    runBlocking {
        val channel = Channel<Int> (capacity = 10, onBufferOverflow = BufferOverflow.SUSPEND)

        val job1 = launch {
            repeat(100) {
                println("Send data $it to Channel ")
                channel.send(it)
            }
        }
        val job2 = launch {
            repeat(10) {
                println("Receive Data ${channel.receive()}")
            }
        }
        joinAll(job1, job2)
        channel.close()
    }
}

fun testChannelBufferOverflowDropLasest(){
    runBlocking {
        val channel = Channel<Int> (capacity = 10, onBufferOverflow = BufferOverflow.DROP_LATEST)

        val job1 = launch {
            repeat(100) {
                println("Send data $it to Channel ")
                channel.send(it)
            }
        }
        val job2 = launch {
            repeat(10) {
                println("Receive Data ${channel.receive()}")
            }
        }
        joinAll(job1, job2)
        channel.close()
    }
}

fun testChannelBufferOverflowDropOldest(){
    runBlocking {
        val channel = Channel<Int> (capacity = 10, onBufferOverflow = BufferOverflow.DROP_OLDEST)

        val job1 = launch {
            repeat(100) {
                println("Send data $it to Channel ")
                channel.send(it)
            }
        }
        val job2 = launch {
            repeat(10) {
                println("Receive Data ${channel.receive()}")
            }
        }
        joinAll(job1, job2)
        channel.close()
    }
}
fun main() {
    testChannelBufferOverflowDropLasest()
}