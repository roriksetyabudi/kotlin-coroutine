package com.rsb.kotlincoroutine.`10_coroutine_sangat_ringan`

import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import java.util.*
import kotlin.concurrent.thread

fun testThread(){
    repeat(100000){
        thread {
            Thread.sleep(1_000)
            println("Done $it : ${Date()} : ${Thread.currentThread().name} ")
        }
    }
    println("Menunggu")
    Thread.sleep(3_000)
    println("Finish Selesai")
}
fun testCoroutine(){
    repeat(100000) {
        GlobalScope.launch {
            delay(1000)
            println("Done $it : ${Date()} : ${Thread.currentThread().name}")
        }
    }
    println("Menunggu")
    runBlocking {
        delay(3_000)
    }
    println("Finish Selesai")
}

fun main() {
    /**
     * Jika ingin menjalankan function silahkan men disable salah satu ya
     * agar tidak bentrok
     */
    //testThread()
    testCoroutine()
}