package com.rsb.kotlincoroutine.`50_actor`

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.channels.actor
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

fun main() {

    val scope = CoroutineScope(Dispatchers.IO)
    val sendChannel = scope.actor<Int>(capacity = 10) {
        repeat(10) {
            println("Actor Receive DAta ${receive()}")
        }
    }
    val job = scope.launch {
        repeat(10) {
            sendChannel.send(it)
        }
    }
    runBlocking {
        job.join()
    }

}