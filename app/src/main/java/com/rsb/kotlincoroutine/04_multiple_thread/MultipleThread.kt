package com.rsb.kotlincoroutine.`04_multiple_thread`

import java.util.*

fun main() {
    val thread1 = Thread(Runnable {
        println(Date())
        Thread.sleep(2_000)
        println("Finish Thread 1 : ${Thread.currentThread().name} : ${Date()}")
    })

    val thread2 = Thread(Runnable {
        println(Date())
        Thread.sleep(2_000)
        println("Finish Thread 2 : ${Thread.currentThread().name} : ${Date()}")
    })

    thread1.start()
    thread2.start()

    println("Menunggu Selesai")
    Thread.sleep(3_000)
    println("Selesai")
}