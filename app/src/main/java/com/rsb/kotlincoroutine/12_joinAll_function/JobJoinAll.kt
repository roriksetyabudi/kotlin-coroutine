package com.rsb.kotlincoroutine.`12_joinAll_function`

import kotlinx.coroutines.*

fun testJobJoinAll(){
    runBlocking {
        val job1: Job = GlobalScope.launch {
            delay(1_000)
            println("Coroutine Done Job 1 : ${Thread.currentThread().name}")
        }
        val job2: Job = GlobalScope.launch {
            delay(2_000)
            println("Coroutine Done Job 2 : ${Thread.currentThread().name}")
        }
        val job3: Job = GlobalScope.launch {
            delay(3_000)
            println("Coroutine Done Job 3 : ${Thread.currentThread().name}")
        }
        joinAll(job1, job2, job3)

    }
}

fun main() {
    testJobJoinAll()
}