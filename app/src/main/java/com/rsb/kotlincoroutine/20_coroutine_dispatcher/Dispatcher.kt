package com.rsb.kotlincoroutine.`20_coroutine_dispatcher`

import kotlinx.coroutines.*

fun testDispatcher(){
    runBlocking {
        println("runBlocking ${Thread.currentThread().name}")

        val job1 = GlobalScope.launch(Dispatchers.Default) {
            println("Jobs 1 ${Thread.currentThread().name}")
        }
        val job2 = GlobalScope.launch(Dispatchers.IO) {
            println("Jobs 2 ${Thread.currentThread().name}")
        }
        joinAll(job1, job2)
    }
}
fun testUnconfined(){
    runBlocking {
        println("RUnblocking ${Thread.currentThread().name}")
        GlobalScope.launch(Dispatchers.Unconfined) {
            println("Uncofined : ${Thread.currentThread().name}")
            delay(1_000)
            println("Uncofined : ${Thread.currentThread().name}")
            delay(1_000)
            println("Uncofined : ${Thread.currentThread().name}")
        }
        GlobalScope.launch {
            println("Confined : ${Thread.currentThread().name}")
            delay(1_000)
            println("Confined : ${Thread.currentThread().name}")
            delay(1_000)
            println("Confined : ${Thread.currentThread().name}")
        }
        delay(3_000)
    }
}

fun main() {
    testUnconfined()
}