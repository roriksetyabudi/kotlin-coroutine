package com.rsb.kotlincoroutine.`20_coroutine_dispatcher`

import kotlinx.coroutines.*
import java.util.concurrent.Executors

fun main() {
    val dispatcherService = Executors.newFixedThreadPool(10).asCoroutineDispatcher()
    val dispatcherWeb = Executors.newSingleThreadExecutor().asCoroutineDispatcher()

    runBlocking {
        val job1 = GlobalScope.launch(dispatcherService) {
            println("Job 1 : ${Thread.currentThread().name}")
        }
        val job2 = GlobalScope.launch(dispatcherWeb) {
            println("Job 2 : ${Thread.currentThread().name}")
        }
        joinAll(job1, job2)
    }
}