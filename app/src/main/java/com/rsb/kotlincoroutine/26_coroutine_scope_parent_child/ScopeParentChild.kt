package com.rsb.kotlincoroutine.`26_coroutine_scope_parent_child`

import kotlinx.coroutines.*
import java.util.concurrent.Executors

fun testScopeParentChildDispatcher(){
    val dispatcher = Executors.newFixedThreadPool(10).asCoroutineDispatcher()
    val scope = CoroutineScope(dispatcher)

    val job = scope.launch {
        println("Parent Scope : ${Thread.currentThread().name}")
        coroutineScope {
            launch {
                println("Child Scope : ${Thread.currentThread().name}")
            }
        }
    }
    runBlocking {
        job.join()
    }
}
fun testScopeParentChildDispatcherCancel(){
    val dispatcher = Executors.newFixedThreadPool(10).asCoroutineDispatcher()
    val scope = CoroutineScope(dispatcher)

    val job = scope.launch {
        println("Parent Scope : ${Thread.currentThread().name}")
        coroutineScope {
            launch {
                delay(2_000)
                println("Child Scope : ${Thread.currentThread().name}")
            }
        }
    }
    runBlocking {
        job.cancelAndJoin()
    }
}
fun main() {
    testScopeParentChildDispatcher()
}