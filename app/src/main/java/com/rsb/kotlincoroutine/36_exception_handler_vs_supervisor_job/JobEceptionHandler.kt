package com.rsb.kotlincoroutine.`36_exception_handler_vs_supervisor_job`

import kotlinx.coroutines.*
import java.lang.IllegalArgumentException
import java.util.concurrent.Executors


fun testJobExceptionHandler(){
    val exceptionHandler = CoroutineExceptionHandler { coroutineContext, throwable ->
        println("Error : ${throwable.message}")
    }
    val dispatcher = Executors.newFixedThreadPool(10).asCoroutineDispatcher()
    val scope = CoroutineScope(dispatcher)

    runBlocking {
        val job = scope.launch { //not use
            launch(exceptionHandler) {
                println("Job Child")
                throw IllegalArgumentException("Child Error")
            }
        }
        job.join()
    }

}

fun testSupervisorJobExceptionHandler(){
    val exceptionHandler = CoroutineExceptionHandler { coroutineContext, throwable ->
        println("Error : ${throwable.message}")
    }
    val dispatcher = Executors.newFixedThreadPool(10).asCoroutineDispatcher()
    val scope = CoroutineScope(dispatcher)

    runBlocking {
        val job = scope.launch { //not use
            supervisorScope {
                launch(exceptionHandler) {
                    println("Job Child")
                    throw IllegalArgumentException("Child Error")
                }
            }
        }
        job.join()
    }

}

fun main() {

    testSupervisorJobExceptionHandler()
}