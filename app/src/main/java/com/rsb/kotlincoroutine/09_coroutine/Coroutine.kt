package com.rsb.kotlincoroutine.`09_coroutine`

import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

suspend fun hello(){
    delay(1_000)
    println("Hello belajar kotlin coroutine")
}
fun main() {
    GlobalScope.launch {
        //silahkan masukan kode program anda di sini
        hello()
    }
    println("Menunggu")
    runBlocking {
        delay(2_000)
    }
    println("SELESAI")
}