package com.rsb.kotlincoroutine.`06_future`

import java.util.concurrent.Callable
import java.util.concurrent.Executors
import java.util.concurrent.Future
import kotlin.system.measureTimeMillis


fun main() {
    val executorService = Executors.newFixedThreadPool(10)
    fun getFoo(): Int {
        Thread.sleep(5_000)
        return 10
    }
    fun getBar(): Int {
        Thread.sleep(5_000)
        return 10
    }

    val time = measureTimeMillis {
        val foo: Future<Int> = executorService.submit(Callable { getFoo() })
        val bar: Future<Int> = executorService.submit(Callable { getBar() })

        val result = foo.get() + bar.get()
        println("Total : $result")
    }
    println("Total Time $time")
}