package com.rsb.kotlincoroutine.`29_name_coroutine`

import kotlinx.coroutines.*

fun main() {
    val scope = CoroutineScope(Dispatchers.IO)
    val job = scope.launch(CoroutineName("Parent")) {
        println("Parent Run In Thread : ${Thread.currentThread().name}")
        withContext(CoroutineName("Child")) {
            println("Child Run In Thread : ${Thread.currentThread().name}")
        }
    }
    runBlocking {
        job.join()
    }
}