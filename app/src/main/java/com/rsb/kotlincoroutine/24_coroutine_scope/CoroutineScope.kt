package com.rsb.kotlincoroutine.`24_coroutine_scope`

import kotlinx.coroutines.*

fun testScope(){
    val scope = CoroutineScope(Dispatchers.IO)
    scope.launch {
        delay(2000)
        println("Run ${Thread.currentThread().name}")

    }
    scope.launch {
        delay(2000)
        println("Run ${Thread.currentThread().name}")

    }
    runBlocking {
        delay(1000)
        println("Done")
    }

}

fun testScopeCancel(){
    val scope = CoroutineScope(Dispatchers.IO)
    scope.launch {
        delay(2000)
        println("Run ${Thread.currentThread().name}")

    }
    scope.launch {
        delay(2000)
        println("Run ${Thread.currentThread().name}")

    }
    runBlocking {
        delay(1000)
        scope.cancel()
        delay(2000)
        println("Done")
    }

}

fun main() {
    testScopeCancel()
}