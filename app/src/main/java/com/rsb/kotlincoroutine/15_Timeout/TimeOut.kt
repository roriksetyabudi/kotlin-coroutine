package com.rsb.kotlincoroutine.`15_Timeout`

import kotlinx.coroutines.*
import java.util.*

fun testTimeout(){
    runBlocking {
        val job = GlobalScope.launch {
            println("Start Coroutine Time Out")
            withTimeout(5000) {
                repeat(100){
                    delay(100)
                    println("$it : ${Date()}")
                }
            }
            println("Finish Coroutine")
        }
        job.join()
    }
}
fun testTimeoutOrNull(){
    runBlocking {
        val job = GlobalScope.launch {
            println("Start Coroutine Time Out")
            withTimeoutOrNull(5000) {
                repeat(100){
                    delay(100)
                    println("$it : ${Date()}")
                }
            }
            println("Finish Coroutine")
        }
        job.join()
    }
}

fun main() {
    testTimeoutOrNull()
}