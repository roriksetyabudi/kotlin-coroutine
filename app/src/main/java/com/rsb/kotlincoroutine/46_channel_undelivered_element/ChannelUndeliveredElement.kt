package com.rsb.kotlincoroutine.`46_channel_undelivered_element`

import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

fun main() {
    val channel = Channel<Int>(capacity = 10) {
        println("Undelivered Element $it")
    }
    channel.close()
    runBlocking {
        val job = launch {
            channel.send(10)
            channel.send(100)
        }
        job.join()
    }
}