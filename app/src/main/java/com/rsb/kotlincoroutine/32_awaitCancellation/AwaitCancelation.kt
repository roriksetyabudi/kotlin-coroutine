package com.rsb.kotlincoroutine.`32_awaitCancellation`

import kotlinx.coroutines.*

fun main() {
    runBlocking {
        val job = launch {
            try {
                println("Job Start")
                awaitCancellation()
            } finally {
                println("Cancelled")
            }
        }
        delay(5000)
        job.cancelAndJoin()
    }
}