package com.rsb.kotlincoroutine.`22_withContext_function`

import kotlinx.coroutines.*
import java.util.concurrent.Executors

fun main() {
    val dispatcherClient = Executors.newFixedThreadPool(10).asCoroutineDispatcher()
    runBlocking {
        val job1 = GlobalScope.launch(Dispatchers.IO) {
            println("Job 1 : ${Thread.currentThread().name}")
            withContext(dispatcherClient) {
                println("Jon 2 :  ${Thread.currentThread().name}")
            }
            println("Job 3 : ${Thread.currentThread().name}")
            withContext(dispatcherClient) {
                println("Jon 4 :  ${Thread.currentThread().name}")
            }
        }
        job1.join()

    }
}