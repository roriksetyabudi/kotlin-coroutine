package com.rsb.kotlincoroutine.`23_non_cancellable_context`

import kotlinx.coroutines.*

fun testCancelFinally(){
    runBlocking {
        val job = GlobalScope.launch {
            try {
                println("Start Job")
                delay(1000)
                println("end Job")
            } finally {
                println(isActive)
                delay(1000)
                println("Finally")
            }
        }
        job.cancelAndJoin()
    }
}
fun testCancellable(){
    runBlocking {
        val job = GlobalScope.launch {
            try {
                println("Start Job")
                delay(1000)
                println("end Job")
            } finally {
                withContext(NonCancellable) {
                    println(isActive)
                    delay(1000)
                    println("Finally")
                }
            }
        }
        job.cancelAndJoin()
    }

}

fun main() {
    testCancellable()
}