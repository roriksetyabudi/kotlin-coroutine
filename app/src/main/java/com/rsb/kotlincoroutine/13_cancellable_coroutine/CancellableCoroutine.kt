package com.rsb.kotlincoroutine.`13_cancellable_coroutine`

import kotlinx.coroutines.*
import java.util.*

fun testCanNotCancel(){
    runBlocking {
        val job = GlobalScope.launch {
            println("Start Coroutine ${Date()}")
            Thread.sleep(2_000)
            println("End Coroutine ${Date()}")
        }
        job.cancel()
        delay(3000)
    }
}
fun testCancellable(){
    runBlocking {
        val job = GlobalScope.launch {
            if(!isActive) throw CancellationException()
            println("Start Coroutine Cancel ${Date()}")

            ensureActive()
            Thread.sleep(2_000)

            ensureActive()
            println("End Coroutine Cancel ${Date()}")
        }
        job.cancel()
        delay(3000)
    }
}

fun main() {
    testCanNotCancel()
    //testCancellable()
}