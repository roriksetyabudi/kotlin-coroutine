package com.rsb.kotlincoroutine.`51_ticker_function`

import kotlinx.coroutines.channels.ticker
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import java.util.*

fun main() {
    val receiveChannel = ticker(delayMillis = 1000)
    runBlocking {
        val job = launch {
            repeat(20) {
                receiveChannel.receive()
                println(Date())
            }
        }
        job.join()
    }
}