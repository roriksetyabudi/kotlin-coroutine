package com.rsb.kotlincoroutine.`35_supervisior_scope_function`

import kotlinx.coroutines.*
import java.lang.IllegalArgumentException
import java.util.concurrent.Executors

fun main() {
    val dispatcher = Executors.newFixedThreadPool(10).asCoroutineDispatcher()
    val scope = CoroutineScope(dispatcher + Job())
    runBlocking {
        scope.launch {
            supervisorScope {
                launch {
                    delay(2000)
                    println("Child 2 DIne")
                }
                launch {
                    delay(1000)
                    throw IllegalArgumentException("Child 2 error")
                }
            }
        }
        delay(1000)
    }
}