package com.rsb.kotlincoroutine.`53_state_flow`

import kotlinx.coroutines.*
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.map
import java.util.*

fun main() {

    val scope = CoroutineScope(Dispatchers.IO)
    val stateFlow = MutableStateFlow(0)

    scope.launch {
        repeat(10) {
            println("      Send $it : ${Date()}")
            stateFlow.emit(it)
            delay(1000)

        }
    }
    scope.launch {
        stateFlow.asStateFlow()
            .map { "Receive Job2 : $it : ${Date()}" }
            .collect {
                delay(2000)
                println(it)
            }
    }
    runBlocking {
        delay(21_000)
        scope.cancel()
    }
}