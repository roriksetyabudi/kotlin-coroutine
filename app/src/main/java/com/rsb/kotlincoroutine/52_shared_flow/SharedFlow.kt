package com.rsb.kotlincoroutine.`52_shared_flow`

import kotlinx.coroutines.*
import kotlinx.coroutines.flow.*
import java.util.*

fun main() {

    val scope = CoroutineScope(Dispatchers.IO)
    val sharedFlow = MutableSharedFlow<Int>()

    scope.launch {
        repeat(10) {
            println("      Send $it : ${Date()}")
            sharedFlow.emit(it)
            delay(1000)

        }
    }
    scope.launch {
        sharedFlow.asSharedFlow()
            .buffer(10)
            .map { "Receive Job1 : $it : ${Date()}" }
            .collect {
                delay(1000)
                println(it)
            }
    }
    scope.launch {
        sharedFlow.asSharedFlow()
            .buffer(10)
            .map { "Receive Job2 : $it : ${Date()}" }
            .collect {
                delay(2000)
                println(it)
            }
    }
    runBlocking {
        delay(21_000)
        scope.cancel()
    }
}