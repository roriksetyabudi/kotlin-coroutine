package com.rsb.kotlincoroutine.`03_membuat_thread`

import java.util.*

fun main() {
    val runnable = Runnable {
        println(Date())
        Thread.sleep(2_000)
        println("Finish : ${Date()}")
    }
    val thread = Thread(runnable)
    thread.start()
    println("Menunggu Selesai")
    Thread.sleep(3_000)
    println("Selesai ")
}