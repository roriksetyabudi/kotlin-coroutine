package com.rsb.kotlincoroutine.`03_membuat_thread`

import java.util.*
import kotlin.concurrent.thread

fun main() {
    thread(start = true) {
        println(Date())
        Thread.sleep(2_000)
        println("Finish : ${Date()}")
    }
    println("Menunggu Selesai")
    Thread.sleep(3_000)
    println("Selesai ")
}