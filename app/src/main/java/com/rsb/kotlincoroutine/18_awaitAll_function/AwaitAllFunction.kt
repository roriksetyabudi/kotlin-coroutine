package com.rsb.kotlincoroutine.`18_awaitAll_function`

import kotlinx.coroutines.*
import kotlin.system.measureTimeMillis

suspend fun getFoo(): Int{
    delay(1_000)
    return 10
}
suspend fun getBar(): Int {
    delay(1_000)
    return 10
}


fun main() {
    runBlocking {
        val time = measureTimeMillis {
            val foo1: Deferred<Int> = GlobalScope.async { getFoo() }
            val bar1: Deferred<Int> = GlobalScope.async { getBar() }
            val foo2: Deferred<Int> = GlobalScope.async { getFoo() }
            val bar2: Deferred<Int> = GlobalScope.async { getBar() }
            val result = awaitAll(foo1,foo2,bar1,bar2).sum()
            println("Result $result")
        }
        println("Total Time $time")

    }
}