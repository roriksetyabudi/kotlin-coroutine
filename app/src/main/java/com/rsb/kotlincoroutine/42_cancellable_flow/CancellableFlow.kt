package com.rsb.kotlincoroutine.`42_cancellable_flow`

import kotlinx.coroutines.*
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.onEach

suspend fun numberFlow(): Flow<Int> = flow {
    repeat(100) {
        emit(it)
    }
}

fun main() {

    val scope = CoroutineScope(Dispatchers.IO)
    runBlocking {
        val job = scope.launch {
            numberFlow()
                .onEach {
                    if(it > 10) cancel()
                    else println(it)
                }
                .collect()
        }
        job.join()
    }

}