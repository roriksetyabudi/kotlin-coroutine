package com.rsb.kotlincoroutine.`19_coroutine_context`

import kotlinx.coroutines.*
import kotlin.coroutines.CoroutineContext

fun main() {
    runBlocking {
        val job = GlobalScope.launch {
            val context: CoroutineContext = coroutineContext
            println(context)
            println(context[Job])
        }
        job.join()
    }
}