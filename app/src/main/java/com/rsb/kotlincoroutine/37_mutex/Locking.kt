package com.rsb.kotlincoroutine.`37_mutex`

import kotlinx.coroutines.*
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import java.util.concurrent.Executors


fun testRaceCondition(){
    var counter: Int = 0
    val dispatcher = Executors.newFixedThreadPool(10).asCoroutineDispatcher()
    val scope = CoroutineScope(dispatcher)

    repeat(100) {
        scope.launch {
            repeat(1000) {
                counter++
            }
        }
    }
    runBlocking {
        delay(5000)
        println("Total Counter : $counter")
    }
}

fun testMutex(){
    var counter: Int = 0
    val dispatcher = Executors.newFixedThreadPool(10).asCoroutineDispatcher()
    val scope = CoroutineScope(dispatcher)
    val mutex = Mutex()

    repeat(100) {
        scope.launch {
            mutex.withLock {
                repeat(1000) {
                    counter++
                }
            }
        }
    }
    runBlocking {
        delay(5000)
        println("Total Counter : $counter")
    }
}

fun main() {
    testMutex()
}