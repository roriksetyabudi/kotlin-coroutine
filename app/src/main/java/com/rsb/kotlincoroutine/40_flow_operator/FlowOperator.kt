package com.rsb.kotlincoroutine.`40_flow_operator`

import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.runBlocking

suspend fun numberFlow(): Flow<Int> = flow {
    repeat(100) {
        emit(it)
    }
}
suspend fun changeToString(number: Int) : String {
    delay(100)
    return "Number $number"
}

fun testFlowOperator(){
    runBlocking {
        val flow1 = numberFlow()
        flow1.filter { it % 2 == 0 }
            .map { changeToString(it) }
            .collect { println(it) }
    }
}

fun main() {
    testFlowOperator()
}