package com.rsb.kotlincoroutine.`54_select_function`

import kotlinx.coroutines.*
import kotlinx.coroutines.channels.produce
import kotlinx.coroutines.selects.select


fun testSelectDeffered(){
    val scope = CoroutineScope(Dispatchers.IO)
    val deffered1 = scope.async {
        delay(1000)
        1000
    }
    val deffered2 = scope.async {
        delay(2000)
        2000
    }
    val deffered3 = scope.async {
        delay(500)
        500
    }
    val job = scope.launch {
        val win = select<Int> {
            deffered1.onAwait{ it }
            deffered2.onAwait{ it }
            deffered3.onAwait{ it }
        }
        println("Win $win")
    }
    runBlocking {
        job.join()
    }
}

fun testSelectChannel(){
    val scope = CoroutineScope(Dispatchers.IO)
    val receiveChannel1 = scope.produce {
        delay(1000)
        send(1000)
    }
    val receiveChannel2 = scope.produce {
        delay(2000)
        send(2000)
    }
    val receiveChannel3 = scope.produce {
        delay(500)
        send(500)
    }
    val job = scope.launch {
        val win = select<String> {
            receiveChannel1.onReceive{ "Result $it" }
            receiveChannel2.onReceive{ "Result $it" }
            receiveChannel3.onReceive{ "Result $it" }
        }
        println("Win $win")
    }
    runBlocking {
        job.join()
    }
}


fun testSelectChannelAndDeffered(){
    val scope = CoroutineScope(Dispatchers.IO)
    val receiveChannel1 = scope.produce {
        delay(1000)
        send(1000)
    }
    val deferred2 = scope.async {
        delay(2000)
        200
    }
    val deferred3 = scope.async {
        delay(500)
        500
    }
    val job = scope.launch {
        val win = select<String> {
            receiveChannel1.onReceive{ "Result $it" }
            deferred2.onAwait{ "Result $it" }
            deferred3.onAwait{ "Result $it" }
        }
        println("Win $win")
    }
    runBlocking {
        job.join()
    }
}

fun main() {
    testSelectChannelAndDeffered()
}