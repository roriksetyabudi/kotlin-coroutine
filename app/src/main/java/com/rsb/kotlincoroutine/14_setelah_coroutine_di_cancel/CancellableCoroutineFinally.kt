package com.rsb.kotlincoroutine.`14_setelah_coroutine_di_cancel`

import kotlinx.coroutines.*
import java.util.*

fun testCancellableFinaly(){
    runBlocking {
        val job = GlobalScope.launch {
            try {
                println("Start Coroutine ${Date()}")
                delay(2_000)
                println("End Coroutine ${Date()}")
            } finally {
                println("Finish")
            }
        }
        job.cancelAndJoin()

    }
}

fun main() {
    testCancellableFinaly()
}