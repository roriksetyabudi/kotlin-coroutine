package com.rsb.kotlincoroutine.`47_produce_function`

import kotlinx.coroutines.*
import kotlinx.coroutines.channels.ReceiveChannel
import kotlinx.coroutines.channels.produce

fun main() {
    val scope = CoroutineScope(Dispatchers.IO)

    val channel: ReceiveChannel<Int> = scope.produce {
        repeat(100) {
            send(it)
        }
    }
    val job1 = scope.launch {
        repeat(100) {
            println(channel.receive())
        }
    }
    runBlocking {
        joinAll(job1)
    }
}