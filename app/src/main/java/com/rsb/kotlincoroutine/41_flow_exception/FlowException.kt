package com.rsb.kotlincoroutine.`41_flow_exception`

import kotlinx.coroutines.flow.*
import kotlinx.coroutines.runBlocking

suspend fun numberFlow(): Flow<Int> = flow {
    repeat(100) {
        emit(it)
    }
}

fun main() {
    runBlocking {
        numberFlow()
            .map { check(it < 10); it }
            .onEach { println(it) }
            .catch { println("Error ${it.message}") }
            .onCompletion { println("Done") }
            .collect()

    }
}