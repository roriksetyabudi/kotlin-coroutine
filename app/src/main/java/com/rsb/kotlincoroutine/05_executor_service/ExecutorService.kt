package com.rsb.kotlincoroutine.`05_executor_service`

import java.util.*
import java.util.concurrent.Executors

fun main() {
    val executorService = Executors.newSingleThreadExecutor()
    repeat(10){
        val runnable = Runnable {
            Thread.sleep(1_000)
            println("Done $it ${Thread.currentThread().name} : ${Date()}")
        }
        executorService.execute(runnable)
        println("Selesai Memasukan Runnable $it")
    }
    println("Menunggu")
    Thread.sleep(11_000)
    println("Finish")
}