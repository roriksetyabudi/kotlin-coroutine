package com.rsb.kotlincoroutine.`39_asynchronous_flow`

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.runBlocking

fun main() {
    val flow1: Flow<Int> = flow {
        println("Star Flow")
        repeat(100) {
            println("Emit It")
            emit(it)
        }
    }
    runBlocking {
        flow1.collect {
            println("Receive It : $it")
        }
    }
}