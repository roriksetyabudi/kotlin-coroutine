package com.rsb.kotlincoroutine.`02_thread_utama`

class ThreadUtama {
    val greating: String
        get() {
            return "Belajar Coroutine"
        }
}

fun main(argument: Array<String>) {
    println(ThreadUtama().greating)
    //cek kita berjalan/running di thread apa
    println("Running in Thread ${Thread.currentThread().name}")
}