package com.rsb.kotlincoroutine.`11_job`

import kotlinx.coroutines.*

fun testJob(){
    runBlocking {
        GlobalScope.launch {
            delay(2_000)
            println("Coroutine Done ${Thread.currentThread().name}")
        }

    }
}

fun testJobLazy(){
    runBlocking {
        val job: Job = GlobalScope.launch(start = CoroutineStart.LAZY) {
            delay(2_000)
            println("Coroutine Done ${Thread.currentThread().name}")
        }
        job.start()
        delay(3_000)

    }
}

fun testJobJoin(){
    runBlocking {
        val job: Job = GlobalScope.launch {
            delay(2_000)
            println("Coroutine Done ${Thread.currentThread().name}")
        }
        job.join()

    }
}

fun testJobCancel(){
    runBlocking {
        val job: Job = GlobalScope.launch {
            delay(2_000)
            println("Coroutine Done ${Thread.currentThread().name}")
        }
        job.cancel()
        delay(3_000)

    }
}

fun main() {
    //testJob()
    //testJobLazy()
    //testJobJoin()
    testJobCancel()
}