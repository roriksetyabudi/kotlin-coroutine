package com.rsb.kotlincoroutine.`16_sequential_suspend_function`

import kotlinx.coroutines.*
import kotlin.system.measureTimeMillis

suspend fun getFoo(): Int{
    delay(1_000)
    return 10
}
suspend fun getBar(): Int {
    delay(1_000)
    return 10
}
fun testSequentialSuspendFunction(){
    runBlocking {
        val time = measureTimeMillis {
            getFoo()
            getBar()
        }
        println("Total Time : $time")
    }
}
fun testSequentialSuspendFunctionCoroutine(){
    runBlocking {
        val job = GlobalScope.launch {
            val time = measureTimeMillis {
                getFoo()
                getBar()
            }
            println("Total Time : $time")
        }
        job.join()
    }
}
fun testConcurrent(){
    runBlocking {
        val time = measureTimeMillis {
            val job1 = GlobalScope.launch { getFoo() }
            val job2 = GlobalScope.launch { getBar() }
            joinAll(job1, job2)
        }
        println("Total TIme : $time")

    }

}
fun main() {
    testConcurrent()
}