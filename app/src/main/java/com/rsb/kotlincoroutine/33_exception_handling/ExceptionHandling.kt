package com.rsb.kotlincoroutine.`33_exception_handling`

import kotlinx.coroutines.*
import java.lang.IllegalArgumentException

fun testExceptionLaunch(){
    runBlocking {
        val job = GlobalScope.launch {
            println("Start Coroutine")
            throw IllegalArgumentException()
        }
        job.join()
        println("Finish")
    }
}

fun testExceptionAsync(){
    runBlocking {
        val deferred = GlobalScope.async {
            println("Start Coroutine")
            throw IllegalArgumentException()
        }
        try {
            deferred.await()
        } catch (error: IllegalArgumentException) {
            println("Error")
        } finally {
            println("Finish")
        }

    }
}

fun testExceptionHandler(){
    val exceptionHandler = CoroutineExceptionHandler { coroutineContext, throwable ->
        println("Ups Error : ${throwable.message}")
    }
    val scope = CoroutineScope(Dispatchers.IO + exceptionHandler)
    runBlocking {
        val job1 = GlobalScope.launch(exceptionHandler) {
            println("Start Coroutine")
            throw IllegalArgumentException("Error")
        }
        job1.join()
        println("Finish")

        val job2 = scope.launch {
            println("Start Coroutine")
            throw IllegalArgumentException("Error")
        }
        job2.join()
        println("Finish")
    }
}

fun main() {
    testExceptionHandler()
}