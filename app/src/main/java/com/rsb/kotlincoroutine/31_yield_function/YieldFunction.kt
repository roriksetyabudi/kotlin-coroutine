package com.rsb.kotlincoroutine.`31_yield_function`

import kotlinx.coroutines.*
import java.util.concurrent.Executors
suspend fun runJob(number: Int) {
    println("Start Job $number in thread :${Thread.currentThread().name}")
    yield()
    println("End Job $number in thread :${Thread.currentThread().name}")
}
fun main() {

    var dispatcher = Executors.newFixedThreadPool(10).asCoroutineDispatcher()
    val scope = CoroutineScope(dispatcher)
    runBlocking {
        scope.launch { runJob(1) }
        scope.launch { runJob(2) }

        delay(2000)
    }


}