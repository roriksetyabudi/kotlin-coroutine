package com.rsb.kotlincoroutine.`44_channel_backpreassure`

import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.joinAll
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

fun testChannelUnlimited(){
    runBlocking {
        val channel = Channel<Int> (capacity = Channel.UNLIMITED) //sesuaikan dengan keinginan
        val job1 = launch {
            println("Send data 1 to Channel")
            channel.send(1)
            println("send data 2 to channel")
            channel.send(2)
        }
        val job2 = launch {
            println("Receive Data : ${channel.receive()}")
            println("Receive Data : ${channel.receive()}")
        }
        joinAll(job1, job2)
        channel.close()
    }
}

fun testChannelConflated(){
    runBlocking {
        val channel = Channel<Int> (capacity = Channel.CONFLATED) //sesuaikan dengan keinginan
        val job1 = launch {
            println("Send data 1 to Channel")
            channel.send(1)
            println("send data 2 to channel")
            channel.send(2)
        }
        val job2 = launch {
            println("Receive Data : ${channel.receive()}")
            println("Receive Data : ${channel.receive()}")
        }
        joinAll(job1, job2)
        channel.close()
    }
}

fun main() {
    testChannelUnlimited()
}