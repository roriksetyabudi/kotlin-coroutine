package com.rsb.kotlincoroutine.`30_gabung_context_element`

import kotlinx.coroutines.*
import java.util.concurrent.Executors

fun main() {
    val dispatcher = Executors.newFixedThreadPool(10).asCoroutineDispatcher()
    val scope = CoroutineScope(Dispatchers.IO + CoroutineName("Test"))
    val job = scope.launch(CoroutineName("Parent") + dispatcher) {
        println("Parent Run In Thread : ${Thread.currentThread().name}")
        withContext(CoroutineName("Child") + Dispatchers.IO) {
            println("Child Run In Thread : ${Thread.currentThread().name}")
        }
    }
    runBlocking {
        job.join()
    }
}