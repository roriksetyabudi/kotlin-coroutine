package com.rsb.kotlincoroutine.`17_async_function`

import kotlinx.coroutines.*
import kotlin.system.measureTimeMillis

suspend fun getFoo(): Int{
    delay(1_000)
    return 10
}
suspend fun getBar(): Int {
    delay(1_000)
    return 10
}
fun main() {
    runBlocking {
        val time = measureTimeMillis {
            val foo: Deferred<Int> = GlobalScope.async { getFoo() }
            val bar: Deferred<Int> = GlobalScope.async { getBar() }
            val result = foo.await() + bar.await()
            println("Result $result")
        }
        println("Total Time $time")

    }

}